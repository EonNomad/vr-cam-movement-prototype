﻿using UnityEngine;
using System.Collections;

public class GoToWaypoint : MonoBehaviour {
    public GameObject[] Waypoints= new GameObject[4];
    public GameObject CameraGO;
    public float speed;
    float step;

    public int WaypointCounter;
    public bool CanGoToNextWaypoint;

	// Use this for initialization
	void Start () {
        CameraGO = this.gameObject;
        step = speed * Time.deltaTime;
        WaypointCounter = 0;
        CanGoToNextWaypoint = true;
	}
	
	// Update is called once per frame
	void Update () {

        if(CanGoToNextWaypoint == true)
        {
            if(WaypointCounter == 0)
            {
                MoveToNextPoint(0);
            }
            if (WaypointCounter == 1)
            {
                MoveToNextPoint(1);
            }

        }
        
        //CameraGO.transform.position = Vector3.MoveTowards(CameraGO.transform.position, Waypoints[0].transform.position, step);
    }

    public void MoveToNextPoint(int number)
    {
        CameraGO.transform.position = Vector3.MoveTowards(CameraGO.transform.position, Waypoints[number].transform.position, step);

        if (CameraGO.transform.position == Waypoints[number].transform.position)
        {
            CanGoToNextWaypoint = false;
            WaypointCounter += 1;
        }
    }
}
