﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using UnityEngine.UI;


public class MyPlayButton : MonoBehaviour {

	[SerializeField] private Material m_NormalMaterial;                
	[SerializeField] private Material m_OverMaterial;                  
	[SerializeField] private Material m_ClickedMaterial;               
	[SerializeField] private Material m_DoubleClickedMaterial;         
	[SerializeField] private VRInteractiveItem m_InteractiveItem;
	[SerializeField] private Renderer m_Renderer;

	public VideoController2 VidCtrlScript;
	public float itemCountdown = 3;
	private SpriteRenderer currentColorSR;
	public Color startColor;
	public Color endColor;

	public GameObject reticleGO;
	public TextMesh myText;
	public GameObject startManagerGO;
	public GameObject musicGO;
	//public RenderHeads.Media.AVProVideo.MediaPlayer introVid;
	public GameObject IntroFrameReg;
	public GameObject IntroFrameOver;

	//public bool isGazing = false;


	// Use this for initialization
	void Start () {
		currentColorSR = this.gameObject.GetComponent<SpriteRenderer>();
//		introVid.Control.Play ();
//		introVid.Control.Pause ();
		//StartCoroutine ("WaitToSeek");
		//Debug.Log ("start coroutine");
		//Debug.Log("is paused: " + introVid.Control.IsPaused() );

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void Awake ()
	{
		//m_Renderer.material = m_NormalMaterial;
	}

//	IEnumerator WaitToSeek(){
//		yield return new WaitForSeconds(.1);
//		introVid.Control.Seek (2*1000);
//		introVid.Control.Stop ();
//		Debug.Log ("seek at coroutine");
//	}

	private void OnEnable()
	{
		m_InteractiveItem.OnOver += HandleOver;
		m_InteractiveItem.OnOut += HandleOut;
		m_InteractiveItem.OnClick += HandleClick;
		m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
	}


	private void OnDisable()
	{
		m_InteractiveItem.OnOver -= HandleOver;
		m_InteractiveItem.OnOut -= HandleOut;
		m_InteractiveItem.OnClick -= HandleClick;
		m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
	}


	//Handle the Over event
	private void HandleOver()
	{
		Debug.Log("handleOver called");
		//if looking for first time, activate
			//m_Renderer.material = m_OverMaterial;
			//this.gameObject.GetComponent<SpriteRenderer>().color= Color.green;
		StartCoroutine("StayOnItem");
//		introVid.Control.Seek (2*1000);
//		introVid.Control.Play ();
		IntroFrameReg.SetActive(false);
		IntroFrameOver.SetActive (true);
		Debug.Log("StayOnItem called");
	}

	IEnumerator StayOnItem(){
		Debug.Log("In couroutine ");

		float elapsedTime = 0.0f;
		float totalTime = 2.0f;

		while(elapsedTime< totalTime){
			itemCountdown -= Time.deltaTime;

			elapsedTime+= Time.deltaTime;
			yield return null;
		}
			
		if (itemCountdown <= 0f) {
			VidCtrlScript.isPlayButtonLookedAt = true;
			//StartCoroutine("DelayTitleCardTimer");
			VidCtrlScript.theMediaPlayer.Control.Play ();

			//coroutine
			StartCoroutine("DelayStartManager");

			reticleGO.SetActive (false);
			//this.gameObject.transform.parent.gameObject.SetActive (false);

//			musicGO.SetActive (true);
//			startManagerGO.SetActive (true);
//			this.gameObject.transform.parent.gameObject.SetActive (false);
			Debug.Log ("Play button looked at");
		}

	}

	IEnumerator DelayTitleCardTimer(){
		yield return new WaitForSeconds(2f);
		VidCtrlScript.isPlayButtonLookedAt = true;

	}

	IEnumerator DelayStartManager(){
		yield return new WaitForSeconds(.1f);
		musicGO.SetActive (true);
		startManagerGO.SetActive (true);
		Debug.Log ("Start Manager delayed");
		this.gameObject.transform.parent.gameObject.SetActive (false);
	}


	//Handle the Out event
	private void HandleOut()
	{
		StopCoroutine ("StayOnItem");
		itemCountdown = 2f;
//		introVid.Control.Pause ();
//		introVid.Control.Seek (2*1000);
		IntroFrameReg.SetActive(true);
		IntroFrameOver.SetActive (false);

		//currentColorSR.color = startColor;
		//myText = itemCountdown;
		//myText.text = "Look at icon to start";
		Debug.Log("Show out state");
		//m_Renderer.material = m_NormalMaterial;
		//CancelInvoke or Couroutine
	}


	//Handle the Click event
	private void HandleClick()
	{
		Debug.Log("Show click state");
		//m_Renderer.material = m_ClickedMaterial;
	}


	//Handle the DoubleClick event
	private void HandleDoubleClick()
	{
		Debug.Log("Show double click");
		//m_Renderer.material = m_DoubleClickedMaterial;
	}


}
	
