﻿using UnityEngine;
using System.Collections;

public class ExtIntSpheres : MonoBehaviour {
	public GameObject[] sphereGameObjects;
	public AudioSource[] sphereAudio;
	MeshRenderer ExtSphereRend;
	MeshRenderer IntSphereRend;
	AudioSource ExtSphereAudio;
	AudioSource IntSphereAudio;

	// Use this for initialization
	void Start () {
		ExtSphereRend = sphereGameObjects [0].GetComponent<MeshRenderer>();
		IntSphereRend = sphereGameObjects [1].GetComponent<MeshRenderer>();

		ExtSphereAudio = sphereAudio [0].GetComponent<AudioSource> ();
		IntSphereAudio = sphereAudio [1].GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyUp (KeyCode.Space)) {
			//play 1st vid
			//SpaceBarPressed(10);
			if (ExtSphereRend.enabled == true) {
				ExtSphereRend.enabled = false;
				IntSphereRend.enabled = true;

				ExtSphereAudio.mute = true;
				IntSphereAudio.mute = false;

			} else if (ExtSphereRend.enabled == false) {
				ExtSphereRend.enabled = true;
				IntSphereRend.enabled = false;

				ExtSphereAudio.mute = false;
				IntSphereAudio.mute = true;
			}
		}
	}
}
