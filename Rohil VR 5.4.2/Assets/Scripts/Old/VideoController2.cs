﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent (typeof(AudioSource))]

public class VideoController2 : MonoBehaviour {
	
	//public MovieTexture[] movieList = new MovieTexture[4];
	public RenderHeads.Media.AVProVideo.MediaPlayer theMediaPlayer;
	//public MovieTexture currMovie;
	AudioSource currAudio;
	AudioSource clickSFX;

	//***Zombie Meter***
	//float playerRunMeter;
	public int startingZombieMeter = 15;
	public int maxZombieMeter = 30;
	public int currentZombie;
	public Slider ZombieSlider;
	public Image damageImage;
	public float flashSpeed = 5f;
	//color(R,G,B, Alpha)
	public Color flashColor = new Color (1f, 0f, 0f, 0.1f);
	
	//works as isDead bool in tut
	//bool ZombieMeterEmpty;
	//works as take damage in tut
	//bool gettingDamage;
	
	//public UnityStandardAssets.ImageEffects.Bloom bloomScript;
	//public UnityStandardAssets.ImageEffects.BloomOptimized bloomScript;
	//public MoviePlayer moviePlayerScript;

	public GameObject[] UIText = new GameObject[4] ;

	//public GameObject darkRoomGO;

	public float myTimer = 0f;
	public bool isPlayButtonLookedAt = false;
	
	//http://unity3d.com/files/docs/sample.ogg
	
	// Use this for initialization
	void Start () {
		//var url2 = "http://unity3d.com/files/docs/sample.ogg";
		// Start download
		//WWW www2 = new WWW(url2);
		//movieList [2] = www2.movie;
//		while (!movieList[2].isReadyToPlay){
//			StartCoroutine(WaitForRequest(www2));
//		}
		
		//To look at object
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = true;
		

		//GetComponent<Renderer> ().material.mainTexture = movieList[0] as MovieTexture;
		//currMovie = movieList [0];
//		currAudio = GetComponent<AudioSource> ();
		//clickSFX = GameObject.Find ("ClickSFX").GetComponent<AudioSource>();
		
		//currAudio.clip = currMovie.audioClip;

//		currAudio.Play ();
//		currAudio.time = 10;
		//currMovie.Play ();

		//moviePlayerGO [0].SetActive(true);
		//currentMoviePlayerGO = moviePlayerGO [0];
		
		//movieList [0].loop = true;

	}
	
	// Update is called once per frame
	void Update () {
		//meter decreasing
		//if (gettingDamage) {
	//		damageImage.color = flashColor;
	//	} else {
	//		damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
	//	}
	//	gettingDamage = false;
		
		//
		//		if (!currMovie.isPlaying){
		//			GetComponent<Renderer> ().material.mainTexture = movieList [0] as MovieTexture;
		//			currMovie.Stop ();
		//			currMovie = movieList [0];
		//			currMovie.Play ();
		//			currAudio.clip = currMovie.audioClip;
		//			currAudio.Play ();
		//
		//
		//		}
		
		//Debug.Log(currAudio.time);

		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = true;


		//Below didn't work

//		if(theMediaPlayer.Control.IsFinished() == true){
//			UIText [5].SetActive (false);
//			myTimer = 0f + Time.deltaTime;
//			Debug.Log ("Loop now, time reset, last image off");
//		}
		if (isPlayButtonLookedAt == true) {
			myTimer = myTimer + Time.deltaTime;
			//Debug.Log(myTimer);
			UIText [0].SetActive (true);
			//darkRoomGO.SetActive (true);
		}

		if (theMediaPlayer.Control.IsFinished() == true) {
			SceneManager.LoadScene ("GearVR App");
			Debug.Log ("REstart!");
		}

		if(myTimer >= 1){
			//1st image off, 2nd On
			UIText [0].SetActive (false);
			UIText [1].SetActive (true);
		}

		if(myTimer >= 3){
			//2nd image off, 3rd On
			UIText [1].SetActive (false);
			UIText [2].SetActive (true);
		}

		if(myTimer >= 7){
			//3rd off, 4th image on
			UIText [2].SetActive (false);
			UIText [3].SetActive (true);
		}
		if(myTimer >= 14.5){
			//4th image off
			UIText [3].SetActive (false);
			//darkRoomGO.SetActive (false);
		}

		if(myTimer >= 28){
			//5th image on
			UIText [4].SetActive (true);
			//darkRoomGO.SetActive (true);
		}

		if(myTimer >= 32.5){
			//5th image off
			UIText [4].SetActive (false);
			//darkRoomGO.SetActive (false);
		}
		if(myTimer >= 39){
			//6th image on
			UIText [5].SetActive (true);
			//darkRoomGO.SetActive (true);
		}

		if(myTimer >= 44){
			//6th image off
			UIText [5].SetActive (false);
			//darkRoomGO.SetActive (false);
		}

		if(myTimer >= 58){
			//7th image on
			UIText [6].SetActive (true);
			//darkRoomGO.SetActive (true);
		}

		if(myTimer >= 65.8){
			//7th image off
			UIText [6].SetActive (false);
			//darkRoomGO.SetActive (false);
		}

		if(myTimer >= 259){
			//8th image on
			UIText [7].SetActive (true);
			//darkRoomGO.SetActive (true);
		}

		if(myTimer >= 264){
			//8th image off, 9th on
			UIText [7].SetActive (false);
			UIText [8].SetActive (true);
		}
		
	} //end Update
	
//	IEnumerator WaitForRequest(WWW www2)
//	{
//		yield return www2;
//	}
	
	void SpaceBarPressed (float amount){
		//Debug.Log ("spacebar was pressed");
		//InvokeRepeating ("BloomOnSpaceBar",.01f,.1f);
		//clickSFX.Play ();
		//gettingCold = true;
//		GetComponent<Renderer> ().material.mainTexture = movieList [amount] as MovieTexture;
//		currMovie.Stop ();
//		currMovie = movieList [amount];
//		currMovie.Play ();
//		currAudio.clip = currMovie.audioClip;
//		currAudio.Play ();
		//moviePlayerScript.videoTime = amount;
		//moviePlayerScript.videoFrame = amount;

		//moviePlayerScript.MovieInstance.PositionSeconds = amount;
		//AVProWindowsMediaPlugin.SeekFrames(-1, amount);
		//next line below works with avpro
		//moviePlayerScript.Control.Seek(amount*1000);
		//moviePlayerScript.Control.Seek
		//Debug.Log ("amount" + amount);
		//moviePlayerScript.s
		//Debug.Log ("spacebar was pressed");


	}

		
}
