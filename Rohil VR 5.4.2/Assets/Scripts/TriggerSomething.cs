﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using UnityEngine.UI;

public class TriggerSomething : MonoBehaviour {

    [SerializeField]
    private Material m_NormalMaterial;
    [SerializeField]
    private Material m_OverMaterial;
    [SerializeField]
    private Material m_ClickedMaterial;
    [SerializeField]
    private Material m_DoubleClickedMaterial;
    [SerializeField]
    private VRInteractiveItem m_InteractiveItem;
    [SerializeField]
    private Renderer m_Renderer;

    public GameObject reticleGO;

    public GameObject activateThisObject;

    public float itemCountdown = 3;

    public GoToWaypoint CameraVRCharacterScript;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnEnable()
    {
        m_InteractiveItem.OnOver += HandleOver;
        m_InteractiveItem.OnOut += HandleOut;
       // m_InteractiveItem.OnClick += HandleClick;
       // m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
    }


    private void OnDisable()
    {
        m_InteractiveItem.OnOver -= HandleOver;
        m_InteractiveItem.OnOut -= HandleOut;
       // m_InteractiveItem.OnClick -= HandleClick;
       // m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
    }

    //Handle the Over event
    private void HandleOver()
    {
        Debug.Log("handleOver called");
        //if looking for first time, activate
        //m_Renderer.material = m_OverMaterial;
        //this.gameObject.GetComponent<SpriteRenderer>().color= Color.green;
        //StartCoroutine("StayOnItem");
     if(this.gameObject.tag == "ActivateObject")
        {
            if (activateThisObject.activeInHierarchy == false)
            {
                activateThisObject.SetActive(true);
            }
            else
            {
                activateThisObject.SetActive(false);
            }
        }

        if (this.gameObject.tag == "MoveTrigger")
        {
            //call function here
            CameraVRCharacterScript.CanGoToNextWaypoint = true;
        }
        //IntroFrameReg.SetActive(false);
        //Debug.Log("StayOnItem called");
    }

    //Handle the Out event
    private void HandleOut()
    {
        StopCoroutine("StayOnItem");
        itemCountdown = 2f;
        //IntroFrameReg.SetActive(true);
        
        //currentColorSR.color = startColor;
        //myText = itemCountdown;
        //myText.text = "Look at icon to start";
       // Debug.Log("Show out state");
        //m_Renderer.material = m_NormalMaterial;
        //CancelInvoke or Couroutine
    }
}
